package br.com.desafio.diazero.dto;

import br.com.desafio.diazero.entity.IncidentesEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IncidentesDTO {

    private Long idIncident;

    @NotNull(message = "name nao pode ser nullo")
    private String name;

    private String description;

    public IncidentesEntity convert(){
        IncidentesEntity entity = new IncidentesEntity();
        entity.setIdIncident(this.idIncident);
        entity.setName(this.name);
        entity.setDescription(this.description);
        return entity;
    }
}
