package br.com.desafio.diazero.api;

import br.com.desafio.diazero.dto.IncidentesDTO;
import br.com.desafio.diazero.service.IncidentesService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/incidentes")
public class IncidentesController {

    private IncidentesService incidentesService;

    public IncidentesController(IncidentesService incidentesService) {
        this.incidentesService = incidentesService;
    }

    @GetMapping
    public ResponseEntity<List<IncidentesDTO>> listAll() {
        return ResponseEntity.ok().body(this.incidentesService.listAll());
    }

    @GetMapping("/paged")
    public ResponseEntity<Page<IncidentesDTO>> pagedList(@RequestParam(name = "page", defaultValue = "0", required = false) int page,
                                                         @RequestParam(name = "size", defaultValue = "20", required = false) int size, @RequestParam(name = "order", defaultValue = "DESC", required = false) String order) {
        return ResponseEntity.ok().body(this.incidentesService.pagedList(page, size, order));
    }

    @GetMapping("/{id}")
    public ResponseEntity<IncidentesDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.incidentesService.findById(id));
    }

    @PostMapping
    public ResponseEntity<IncidentesDTO> insert(@RequestBody @Valid IncidentesDTO incidentesDTO) {
        return ResponseEntity.ok(incidentesService.insert(incidentesDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<IncidentesDTO> update(@RequestBody @Valid IncidentesDTO incidentesDTO, @RequestParam Long id) {
        return ResponseEntity.ok(incidentesService.update(incidentesDTO, id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@RequestParam Long id){
        incidentesService.delete(id);
        return ResponseEntity.ok().build();
    }
}
