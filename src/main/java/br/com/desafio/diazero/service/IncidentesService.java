package br.com.desafio.diazero.service;

import br.com.desafio.diazero.dto.IncidentesDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IncidentesService {

    IncidentesDTO insert(IncidentesDTO dto);

    IncidentesDTO findById(Long id);

    IncidentesDTO update(IncidentesDTO dto, Long id);

    List<IncidentesDTO> listAll();

    Page<IncidentesDTO> pagedList(int page, int size,String order);

    void delete(Long id);
}
