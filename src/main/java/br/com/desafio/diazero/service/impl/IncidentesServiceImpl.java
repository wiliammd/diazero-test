package br.com.desafio.diazero.service.impl;

import br.com.desafio.diazero.dto.IncidentesDTO;
import br.com.desafio.diazero.entity.IncidentesEntity;
import br.com.desafio.diazero.execption.ResourceNotFoundException;
import br.com.desafio.diazero.repository.IncidentesRepository;
import br.com.desafio.diazero.service.IncidentesService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IncidentesServiceImpl implements IncidentesService {

    private IncidentesRepository incidentesRepository;

    public IncidentesServiceImpl(IncidentesRepository incidentesRepository) {
        this.incidentesRepository = incidentesRepository;
    }

    @Override
    public IncidentesDTO insert(IncidentesDTO dto) {
        IncidentesEntity entity = dto.convert();
        entity.setActive(true);
        incidentesRepository.save(entity);
        return dto;
    }

    @Override
    public IncidentesDTO findById(Long id) {
        IncidentesEntity entity = incidentesRepository.findByIdIncidentAndActiveTrue(id).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Incidente de id: %s não encontrado!", id)));
        return entity.convert();
    }

    @Override
    public IncidentesDTO update(IncidentesDTO dto, Long id) {
        IncidentesEntity entity = incidentesRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Incidente de id: %s não encontrado!", id)));
        entity.setDescription(dto.getDescription());
        entity.setName(dto.getName());
        entity.setUpdatedAt(LocalDateTime.now());
        incidentesRepository.save(entity);
        return entity.convert();
    }

    @Override
    public List<IncidentesDTO> listAll() {
        return incidentesRepository.findAllByActiveTrue().stream().map(entity -> entity.convert()).collect(Collectors.toList());
    }

    @Override
    public Page<IncidentesDTO> pagedList(int page, int size, String order) {
        PageRequest pageRequest = PageRequest.of(page,size,direction(order),"createdAt");
        return incidentesRepository.findAllByActiveTrue(pageRequest).map(entity -> entity.convert());
    }

    @Override
    public void delete(Long id) {
        IncidentesEntity entity = incidentesRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Incidente de id: %s não encontrado!", id)));
        entity.setActive(false);
        entity.setClosedAt(LocalDateTime.now());
    }

    private Sort.Direction direction(String order){
        if(order.equals("DESC")){
            return Sort.Direction.DESC;
        }
        else{
            return Sort.Direction.ASC;
        }
    }
}
