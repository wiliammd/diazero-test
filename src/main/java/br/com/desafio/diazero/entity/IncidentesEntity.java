package br.com.desafio.diazero.entity;

import br.com.desafio.diazero.dto.IncidentesDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "INCIDENTES_ENTITY")
public class IncidentesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idIncident;

    private String name;

    private String description;

    @CreationTimestamp
    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private LocalDateTime closedAt;

    private Boolean active;

    public IncidentesDTO convert(){
        IncidentesDTO dto = new IncidentesDTO();
        dto.setIdIncident(this.idIncident);
        dto.setName(this.name);
        dto.setDescription(this.description);
        return dto;
    }
}
