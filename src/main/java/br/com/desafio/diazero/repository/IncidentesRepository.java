package br.com.desafio.diazero.repository;

import br.com.desafio.diazero.entity.IncidentesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IncidentesRepository extends JpaRepository<IncidentesEntity,Long> {

    List<IncidentesEntity> findAllByActiveTrue();
    Optional<IncidentesEntity> findByIdIncidentAndActiveTrue(Long id);
    Page<IncidentesEntity> findAllByActiveTrue(Pageable pageable);
}
