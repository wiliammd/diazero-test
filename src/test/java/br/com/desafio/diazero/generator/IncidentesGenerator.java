package br.com.desafio.diazero.generator;

import br.com.desafio.diazero.dto.IncidentesDTO;
import br.com.desafio.diazero.entity.IncidentesEntity;

public class IncidentesGenerator {

    public static IncidentesDTO createdDto(){
        IncidentesDTO incidentesDTO = new IncidentesDTO();
        incidentesDTO.setIdIncident(1L);
        incidentesDTO.setName("teste");
        incidentesDTO.setDescription("descricao");
        return incidentesDTO;
    }
    public static IncidentesEntity createdEntity(){
        IncidentesEntity incidentesEntity = new IncidentesEntity();
        incidentesEntity.setIdIncident(1L);
        incidentesEntity.setName("teste");
        incidentesEntity.setDescription("descricao");
        return incidentesEntity;
    }
}
