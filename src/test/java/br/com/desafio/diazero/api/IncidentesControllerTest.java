package br.com.desafio.diazero.api;

import br.com.desafio.diazero.service.IncidentesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(IncidentesController.class)
public class IncidentesControllerTest {

    protected MockMvc mockMvc;
    @MockBean
    private IncidentesService incidentesService;
    private IncidentesController incidentesController;

    @BeforeEach
    public void setup()throws InterruptedException{
        FormattingConversionService formattingConversionService = new FormattingConversionService();
        this.incidentesService = Mockito.mock(IncidentesService.class);
        this.incidentesController = new IncidentesController(incidentesService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(incidentesController).setConversionService(formattingConversionService).build();
    }

    @Test
    void listarTodos() throws Exception{
        mockMvc.perform(get("/incidentes")).andExpect(status().isOk());
    }

    @Test
    void delete() throws Exception{
        mockMvc.perform(get("/incidentes/1")).andExpect(status().isOk());
    }
}
