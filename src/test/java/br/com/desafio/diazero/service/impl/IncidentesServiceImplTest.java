package br.com.desafio.diazero.service.impl;

import br.com.desafio.diazero.dto.IncidentesDTO;
import br.com.desafio.diazero.execption.ResourceNotFoundException;
import br.com.desafio.diazero.generator.IncidentesGenerator;
import br.com.desafio.diazero.repository.IncidentesRepository;
import br.com.desafio.diazero.service.IncidentesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class IncidentesServiceImplTest {

    private IncidentesService incidentesService;
    private IncidentesRepository incidentesRepository;

    @BeforeEach
    void setup() {
        incidentesRepository = Mockito.mock(IncidentesRepository.class);
        incidentesService = new IncidentesServiceImpl(incidentesRepository);

    }

    @Test
    void insert() {
        when(incidentesRepository.save(ArgumentMatchers.any())).thenReturn(IncidentesGenerator.createdEntity());
        IncidentesDTO result = incidentesService.insert(IncidentesGenerator.createdDto());
        assertTrue(result != null);
    }

    @Test
    void findById() {
        when(incidentesRepository.findByIdIncidentAndActiveTrue(ArgumentMatchers.any())).thenReturn(Optional.of(IncidentesGenerator.createdEntity()));
        IncidentesDTO result = incidentesService.findById(1L);
        assertTrue(result != null);
    }

    @Test
    void findByIdException() {
        when(incidentesRepository.findByIdIncidentAndActiveTrue(ArgumentMatchers.any())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class,() -> incidentesService.findById(1L));
    }
}